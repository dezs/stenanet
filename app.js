
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

app.disable('x-powered-by'); //http://expressjs.com/en/advanced/best-practice-security.html - probably use Helmet in future
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// IMPORTANT: Your application HAS to respond to GET /health with status 200
//            for OpenShift health monitoring
app.use('/health', function(req, res, next) {  
    res.writeHead(200);
    res.end();
})

// favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon_v2.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


//locale setup
const i18n = require('i18n');
const localesData = require('./common/localization/locales');
i18n.configure(localesData);

app.use(cookieParser());
const maxCookieAge = 1000 * 60 * 60 * 24 * 365;
app.use(function(req, res, next) {
  var lng = req.cookies[localesData.cookie];
  //console.log(lng);
  if (!lng){   
    res.cookie(localesData.cookie, 'ru', { maxAge:maxCookieAge,  httpOnly: false });
  }
  
  i18n.init(req, res, next);
});
//app.use(i18n.init);

//version middleware
var version = require("./common/version");
app.use(version.routeWithVersion);

//database

var db = require('./utils/database');
var dbError;

db.connect(function(err){
  if (err){
    dbError = err;
  }
});

app.use(function(req, res, next) {
  if (dbError){
     console.log('dbErr!', dbError);
     next(new Error('Error with database'));        
  } else{
    next();
  }
});

//user sessions
var expressSession = require('express-session');
var MongoSessionStore = require('connect-mongo')(expressSession);
app.use(expressSession({
    store: new MongoSessionStore(db.getSessionStoreOptions()),
    saveUninitialized: false,
    resave: false,
    name: "doNotLookAtMe",
    secret: "lasduireh438347243230*)AOSDA78q3647yTGGTYA%^asdga"
}));

//routes

var routes = require('./routes/index');
var users = require('./routes/users');
app.use('/', routes);
app.use('/user', users);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
} else{
  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
}


module.exports = app;