var v = {
	version:"0.0.3.0",
	routeWithVersion: function(req, res, next){
		res.locals.version = v.version;
		res.locals.lang = res.getLocale();
		
		next(); 
	}
}

module.exports = v;