$(function(){
    $('#language-menu').click(function lngChange(e) {
        var lang = $(e.target).data('lang');
        
        if (lang){
            e.preventDefault();
            Cookies.set('locale', lang, { expires: 365 });
            location.reload();            
        }
    })
})