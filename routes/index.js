var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.locals.longText = res.__("stenanet_about");
  
  res.render('main/index', { title: res.__('nav_about') });
});

router.get('/partners', function(req, res, next) {
  res.render('main/partners', { title: res.__('Our_partners') });
});

router.get('/how-it-works', function(req, res, next) {
  res.render('main/hiw', { title: res.__("How_it_works") });
});

router.get('/view', function(req, res, next) {
  res.render('main/view', { title: res.__('View all') });
});

module.exports = router;
