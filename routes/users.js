var express = require('express');
var router = express.Router();

var db = require('../utils/database');
/* GET users listing. */
router.get('/', function(req, res, next) {
  
  var userSession = req.session;
  
  userSession.views = ( userSession.views || 0 ) +1;
  
  var users = db.testQuery().then(function(docs){
    console.log(docs)
    res.send('Test query count ' + docs.length + '<br/> Views count : ' + userSession.views);
  })
  
});

router.get('/test', function(req, res, next) {
  db.insertWall({
    name: 'lala',
    description: 'asdasd'
  }).then(function() {
    res.send("insert occured!")
  });
});

module.exports = router;
