var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
//todo: config!
var dbUser = 'stena';
var dbPassword = 's93kjer_IO&#$yre';

var mongoUrl = 'mongodb://'+ dbUser + ':' + dbPassword + '@ds021922.mlab.com:21922/stenanet';
var db;
var isConnected = false;
const wallCollectionName = 'walls';

var ourDb = {};

ourDb.isConnected = function(){
    return isConnected;
}

ourDb.connect = function(callback) {
    MongoClient.connect(mongoUrl, function mongoConnect(err, database) {
        if (err){
            isConnected = false;
            callback(err);
            return;
        }
        
        db = database;
        isConnected = true;
        callback();
    })  
}

ourDb.getSessionStoreOptions = function(){
    return{
        url: mongoUrl,
        collection: "userSessions",
        touchAfter: 60*60
    };
}

ourDb.testQuery = function testQuery(successCallback, errCallback) {
    return db.collection(wallCollectionName).find().toArray();
}

//return promise!
ourDb.insertWall = function insertWall(wallObj){
    return db.collection(wallCollectionName).insertOne(wallObj);
}


module.exports = ourDb;